﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataLayer.Context;
using DataLayer.Entity.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.DTOs.Vote;
using Services.Interfaces;

namespace Asp_Sample.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IVoteService _voteService;
        private readonly AppDbContext _dbContext;

        public HomeController(IVoteService voteService, AppDbContext dbContext)
        {
            _voteService = voteService;
            _dbContext = dbContext;
        }

        public async Task<IActionResult> Index()
        {
            ViewData["Votes"] = await  _voteService.GetAllVote();
            return View();
        }

        [HttpGet]
        public IActionResult AddVote()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddVote(VoteDto model)
        {
            await _voteService.AddVoteAsync(model);
            return View();
        }

        public async Task<IActionResult> ActiveDeActive(Guid id)
        {
            var res = await _dbContext.Votes.FindAsync(id);
            res.Active = !res.Active;

            _dbContext.Update(res);
            await _dbContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> ShowResponse(Guid id)
        {
            ViewData["Response"] = await  _voteService.GetVoteCount(id);
            return View();
        }
    }
}